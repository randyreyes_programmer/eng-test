package eng.technical.test;

public class ConsolePrint {
	
	public  final int CLOSE_APPLICATION= 0;
	public final int VIEW_APPLICATION =1;
	public  final int VIEW_ALL = 2;
	public  final int ADD_PRODUCT = 3;
	public final int MAKE_PURCHASE = 4;
	
	public final int UNLIMTED1GB = 5;
	public final int UNLIMTED2GB = 6;
	public final int UNLIMTED5GB = 7;
	public final int DATAPACK1GB = 8;
	public final int COMPUTETOTAL = 9;
	
	public void printHead(){
		System.out.println("*****************************************");
		System.out.println("* Option*   Main Application                        *");
		System.out.println("*****************************************"); 
		System.out.println("*     1      *  View Main Application               *");
		System.out.println("*     2      *  View all Products                        *");
		System.out.println("*     3      *  Add a product                             *");
		System.out.println("*     4      *  Make a purchase                         *");
		System.out.println("*     0      *  EXIT APPLICATION               *");
		System.out.println("*****************************************");
		System.out.println("*             Kindly select an option                   *");
		System.out.println("*****************************************");
	}
	
	public void printPurchase(){
		System.out.println("*****************************************");
		System.out.println("* Option*   Select Item to Purchase              *");
		System.out.println("*****************************************"); 
		System.out.println("*     5      *  Unlimited 1GB                           *");
		System.out.println("*     6      *  Unlimited 2GB                           *");
		System.out.println("*     7      *  Unlimited 5GB                           *");
		System.out.println("*     8      *  1GB Data-pack                          *");
		System.out.println("*     9      *  Compute Total Purchase            *");
		System.out.println("*     1      *  Back to Main Application          *");
		System.out.println("*****************************************");
		System.out.println("*             Kindly select an option                   *");
		System.out.println("*****************************************");
	}
	
	public void clossingApplication(){
		System.out.println("*****************************************");
		System.out.println("*            Application Closed                          *");
		System.out.println("*****************************************");
	}
}
