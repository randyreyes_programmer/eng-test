package eng.technical.test;

public class MainUtility {
	
	public int validateInputMain(String answer){
		int test = 9;
		
		if(answer != null){
			if(isInputANumber(answer)){
				test = Integer.parseInt(answer);
				if(test<=4){
					return test;
				}
				else{
					System.out.println("Invalid Selection");
					return test;
				}
			}
			else{
				System.out.println("Invalid Selection");
				return test;
			}
		}
		else{
			System.out.println("Invalid Selection");
			return test;
		}
	}
	
	public int validateInputPurchase(String answer){
		int test = 9;
		
		if(answer != null){
			if(isInputANumber(answer)){
				test = Integer.parseInt(answer);
				if(test> 4 && test < 10){
					return test;
				}
				else if (test == 1){
					return test;
				}
				else if (test == 0){
					return test;
				}
				else{	
					System.out.println("Invalid Selection");
					return test;
				}
			}
			else{
				System.out.println("Invalid Selection");
				return test;
			}
		}
		else{
			System.out.println("Invalid Selection");
			return test;
		}
	}
	
	public boolean isInputANumber(String test){
		
		try{
			Integer.parseInt(test);
		}
		catch(NumberFormatException nfe){
			return false;
		}
		
		return true;
	}
	
	public boolean yesOrNo(String test){
		boolean answer = false;
		if(test != null){
			if (test.equalsIgnoreCase("Y")){
				answer =  true;
			}
			else if 	(test.equalsIgnoreCase("N")){
					answer =  false;
			}
		}
		else{
			System.out.println("Invalid Selection");
		}
		
		return answer;
	}
}
