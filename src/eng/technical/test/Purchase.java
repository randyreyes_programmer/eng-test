package eng.technical.test;

public class Purchase {
	private String productCode;
	private String productName;
	private double price;
	private int quantity;
	private boolean promoCode;
	private double total;
	private double discount;
	private String additionalItems;
	
	public Purchase(String productCode, String productName, double price){
		this.productCode = productCode;
		this.productName = productName;
		this.price = price;
	}
	
	public String getProductCode() {return productCode;}
	public void setProductCode(String productCode) {this.productCode = productCode;}
	
	public String getProductName() {	return productName;}
	public void setProductName(String productName) {this.productName = productName;}
	
	public double getPrice() {return price;}
	public void setPrice(double price) {	this.price = price;}
	
	public int getQuantity() {	return quantity;}
	public void setQuantity(int quantity) {this.quantity = quantity;}
	
	public boolean getPromoCode() {return promoCode;}
	public void setPromoCode(boolean promoCode) {this.promoCode = promoCode;}
	
	public double getTotal() {return total;}
	public void setTotal(double total) {	this.total = total;	}
	
	public double getDiscount() {	return discount;}
	public void setDiscount(double discount) {this.discount = discount;}
	
	public String getAdditionalItems() {return additionalItems;}
	public void setAdditionalItems(String additionalItems) {this.additionalItems = additionalItems;	}

	@Override
	public String toString() {
		return "Purchase [productCode=" + productCode + ", productName=" + productName + ", price=" + price
				+ ", quantity=" + quantity + ", promoCode=" + promoCode + ", total=" + total + ", discount=" + discount
				+ ", additionalItems=" + additionalItems + "]";
	}
	
	
	
}
