package eng.technical.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainApplication {

	Cart cart = new ShoppingCart();
	ConsolePrint consolePrint = new ConsolePrint();
	MainUtility application = new MainUtility();
	Scanner console; 
	List<Purchase> purchases = new ArrayList<Purchase>();
	
	int inputNumber; 
	
	public static void main(String[] args){
		MainApplication main = new MainApplication();
		main.startApplication();
		
	}
	public void startApplication(){
		ConsolePrint consolePrint = new ConsolePrint();
		consolePrint.printHead();
		
		System.out.println("Kindly enter your selection:");
		console = new Scanner(System.in);
		
		String answer = console.next();
		inputNumber = application.validateInputMain(answer);
		
		while(inputNumber != consolePrint.CLOSE_APPLICATION){
			
			if(inputNumber == consolePrint.VIEW_APPLICATION){
				consolePrint.printHead();
			}
			
			if(inputNumber == consolePrint.VIEW_ALL){
				System.out.println(cart.getItems());
			}
			if(inputNumber == consolePrint.ADD_PRODUCT){
				addNewProduct();
			}
			
			if(inputNumber == consolePrint.MAKE_PURCHASE){
				consolePrint.printPurchase();
				makeNewPurchase();
			}
			
			System.out.println("Kindly enter your selection:");
			answer = console.next();
			inputNumber = application.validateInputMain(answer);
		}
		if(inputNumber == consolePrint.CLOSE_APPLICATION){
			closeApplication();
		}
	}
	
	public void addNewProduct(){
		String code = null;
		String name = null;
		double price = 0.0;
		System.out.println("Enter new Product Code: ");
		code = console.next();
		System.out.println("Enter new Product Name: ");
		name = console.next();
		System.out.println("Enter new Product Price: ");
		price = console.nextDouble();
		Item item = new Item(code, name, price);
		cart.add(item);
		System.out.println("Product has been successfully added");
		System.out.println(cart.getItems());
	}
	
	public void makeNewPurchase(){

		while(inputNumber != consolePrint.VIEW_APPLICATION){	
			
			if(inputNumber == consolePrint.UNLIMTED1GB){
				addUnlimited1gb();
				consolePrint.printPurchase();
			}
			
			if(inputNumber == consolePrint.UNLIMTED2GB){
				addUnlimited2gb();
				consolePrint.printPurchase();
			}
			
			if(inputNumber == consolePrint.UNLIMTED5GB){
				addUnlimited5gb();
				consolePrint.printPurchase();
			}
			
			if(inputNumber == consolePrint.DATAPACK1GB){
				addDataPack();
				consolePrint.printPurchase();
			}
			
			if(inputNumber == consolePrint.COMPUTETOTAL){
				printTotal();
				consolePrint.printPurchase();
			}
			
			String answer = console.next();
			inputNumber = application.validateInputPurchase(answer);
		}
		
		if(inputNumber == consolePrint.VIEW_APPLICATION){
			consolePrint.printHead();
		}
	}
	
	public void addUnlimited1gb(){
		int qty = 0;
		int total = 0;
		int discount = 0;
		Purchase purchase = new Purchase("ult_small", "Unlimited 1GB",24.9);
		System.out.println("How many items?");
		qty =console.nextInt();
		purchase.setQuantity(qty);
		if(qty >= 3){
			discount = qty /3;
			total = qty - (discount * 3) ;
			purchase.setTotal((purchase.getPrice() * total) + (purchase.getPrice() * discount *2));
		}
		else{
			purchase.setTotal(purchase.getPrice() * qty);
		}
		purchase.setAdditionalItems("");
		purchases.add(purchase);
		System.out.println("Succesfully added to shopping cart");
	}
	
	public void addUnlimited2gb(){
		int qty = 0;
		Purchase purchase = new Purchase("ult_medium", "Unlimited 2GB", 29.9);
		System.out.println("How many items?");
		qty =console.nextInt();
		purchase.setQuantity(qty);
		purchase.setTotal(qty * purchase.getPrice());
		purchase.setAdditionalItems("Free " + qty + " 1GB Data-pack ");
		
		purchases.add(purchase);
		System.out.println("Succesfully added to shopping cart");
	}
	
	public void addUnlimited5gb(){
		int qty = 0;
		double newPrice =39.90;
		Purchase purchase = new Purchase("ult_large","Unlimited 5GB", 44.9);
		System.out.println("How many items?");
		qty =console.nextInt();
		purchase.setQuantity(qty);
		if(qty >= 3){
			purchase.setTotal(newPrice * qty);
		}
		else{
			purchase.setTotal(purchase.getPrice() * qty);
		}
		purchase.setAdditionalItems("");
		purchases.add(purchase);
		System.out.println("Succesfully added to shopping cart");
	}
	
	public void addDataPack(){
		int qty = 0;
		Purchase purchase = new Purchase( "1gb", "1GB Data-pack", 9.9);
		System.out.println("How many items?");
		qty =console.nextInt();
		purchase.setQuantity(qty);
		purchase.setTotal(qty * purchase.getPrice());
		purchase.setAdditionalItems("");
		purchases.add(purchase);
		System.out.println("Succesfully added to shopping cart");
	}
	
	public void printTotal(){
		if (purchases.isEmpty()){
			System.out.println("No purchases have been made");
		}
		else
			System.out.println("Items Purchased");
			for(Purchase p : purchases){
				System.out.println("Product: " + p.getProductName() +"\nQuantity: " + p.getQuantity()+
						"\nTotal cost: " + p.getTotal() + "\nBonus Item: " + p.getAdditionalItems() +
						"\n--------------------------------------------------------------");
			}
	}
	
	public void closeApplication(){
		consolePrint.clossingApplication();
		console.close();
		System.exit(0);
	}
}
