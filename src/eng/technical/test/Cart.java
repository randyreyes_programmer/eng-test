package eng.technical.test;

import java.util.List;

public interface Cart {
	public void  newPromo(String pricingRules);
	public void add(Item item);
	public void add(Item item, String promoCode);
	public void total();
	public List<Item>  getItems();
}

	
	
