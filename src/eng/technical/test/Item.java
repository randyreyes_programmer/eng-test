package eng.technical.test;

public class Item {
	private String productCode;
	private String productName;
	private double price;
	
	public Item(String newProductCode, String newProductName, double newPrice){
		this.productCode = newProductCode;
		this.productName = newProductName;
		this.price = newPrice;
	}
	public String getProductCode() {return productCode;}
	public void setProductCode(String productCode) {this.productCode = productCode;}
	
	public String getProductName() {	return productName;}
	public void setProductName(String productName) {this.productName = productName;}
	
	public double getPrice() {return price;}
	public void setPrice(double price) {	this.price = price;}
	
	@Override
	public String toString() {
		return "Code: " + productCode + "\tName: " + productName + " \tPrice: " + price  + "\n";
	}
}
