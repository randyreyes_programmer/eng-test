package eng.technical.test;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements Cart{
	


	private List<Item> itemList = new ArrayList<Item>();
	
	public ShoppingCart(){
		itemList.add( new Item("ult_small", "Unlimited 1GB",24.9));
		itemList.add(new Item("ult_medium", "Unlimited 2GB", 29.9));
		itemList.add(new Item("ult_large","Unlimited 5GB", 44.9));
		itemList.add(new Item( "1gb", "1GB Data-pack", 9.9));
	}
	
	@Override
	public void newPromo(String pricingRules) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void add(Item item) {
		itemList.add(item);
	}

	@Override
	public void add(Item item, String promoCode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void total() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Item>  getItems() {
		return itemList;
	}

}
